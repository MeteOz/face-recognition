#!/usr/bin/env python3

import os
import sys
import datetime
fileDir = os.path.dirname(os.path.realpath(__file__))

import txaio
txaio.use_twisted()
import pickle 
from twisted.internet import task
from twisted.internet import reactor

from autobahn.twisted.websocket import WebSocketServerProtocol, \
    WebSocketServerFactory
from twisted.internet import task, defer

from twisted.python import log

import argparse
import cv2
import imagehash
import json
from PIL import Image
import numpy as np
import os
import io
from io import StringIO
import urllib
import base64

from sklearn.decomposition import PCA
from sklearn.model_selection import GridSearchCV
from sklearn.manifold import TSNE
from sklearn.svm import SVC

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import dlib
from AlignDlib import AlignDlib

class Face:

    def __init__(self, rep, identity):
        self.rep = rep
        self.identity = identity

    def __repr__(self):
        return "{{id: {}, rep[0:5]: {}}}".format(
            str(self.identity),
            self.rep[0:5]
        )


images = {}
client = None
dimensions = []
people = []
svm = None
trainingImageCount=0;
align =  AlignDlib("shape_predictor_68_face_landmarks.dat")
sp = dlib.shape_predictor("land.dat")
facerec = dlib.face_recognition_model_v1("facenet.dat")

video_capture = cv2.VideoCapture(0)
video_capture.set(3, 640)
video_capture.set(4, 480)


training = False

def loadPickle():
       global training;
       training = True
       global images;
       global dimensions;
       global people;
       try:
           with  open("dima", 'rb')  as f:
               dimensions = pickle.load(f) 
               
               for jsImage in dimensions:
                   h = jsImage['hash'].encode('ascii', 'ignore')
                   images[h] = Face(jsImage['representation'], jsImage['identity'])
       except IOError:
               print("Error: File does not appear to exist.")


       try:
           with  open("people_new", 'rb')  as f:
               people = pickle.load(f)
       except IOError:
           print("Error: File does not appear to exist.")
       
       trainSVM()
       training = False

def getData():
        global images;
        X = []
        y = []
        for img in images.values():
            X.append(img.rep)
            y.append(img.identity)
        numIdentities = len(set(y + [-1])) - 1
        if numIdentities == 0:
            return None

        X = np.vstack(X)
        y = np.array(y)
        return (X, y)


def trainSVM():
        print("+ Training SVM on {} labeled images.".format(len(images)))
        d = getData()
        global svm;
        if d is None:
            svm = None
            return
        else:
            (X, y) = d
            numIdentities = len(set(y + [-1]))
            if numIdentities <= 1:
                return

            param_grid = [
                {'C': [1, 10, 100, 1000],
                 'kernel': ['linear']},
                {'C': [1, 10, 100, 1000],
                 'gamma': [0.001, 0.0001],
                 'kernel': ['rbf']}
            ]
            svm = GridSearchCV(SVC(C=1,probability=True), param_grid, cv=5).fit(X, y)



def infer(frame):

  identities = []
  global images;
  global client;
  global svm;
  global training;
  global trainingImageCount;
  global people;
  global dimensions;

  annotatedFrame = np.copy(frame)
  bb = align.getLargestFaceBoundingBox(frame)
  bbs = [bb] if bb is not None else []
  for bb in bbs:

       landmarks = align.findLandmarks(frame, bb)
       alignedFace = align.align(224, frame, bb,
                                      landmarks=landmarks,
                                      landmarkIndices=AlignDlib.OUTER_EYES_AND_NOSE)
       if alignedFace is None:
             continue

       phash = str(imagehash.phash(Image.fromarray(alignedFace)))
       if phash in images:
             identity = images[phash].identity
       else:
                a = datetime.datetime.now()

                shape = sp(alignedFace,dlib.rectangle(0,0,224,224))
                rep = facerec.compute_face_descriptor(alignedFace,shape)
                if training:

                    identity = len(people) -1;
                    images[phash] = Face(np.array(rep), identity)
                    trainingImageCount = trainingImageCount + 1;
                    content = [str(x) for x in alignedFace.flatten()]
                    msg = {
                        "type": "NEW_IMAGE",
                        "hash": phash,
                        "content": content,
                        "identity": identity
                    }
                    if(client is not None):
                       client.sendMessage(str.encode(json.dumps(msg)))
                    msg["representation"]= np.array(rep)
                    del  msg["content"]

                    dimensions.append(msg)
                    file_pi = open('dima', 'wb') 
                    pickle.dump(dimensions, file_pi)
                    if(trainingImageCount>10):
                          training = False
                          msg = {
                              "type": "TRAINING",
                              "value": False
                          }
                          trainingImageCount = 0;   
                          if(client is not None):
                                  client.sendMessage(str.encode(json.dumps(msg))) 	

          
                else:
                    if len(people) == 0:
                        identity = -1
                    elif len(people) == 1:
                        identity = 0
                    elif svm:
                        prob = svm.predict_proba([np.array(rep)])
                        indice = np.nonzero(prob[0] > 0.25)
                        if(indice[0] > -1):
                         identity = indice[0][0]
                        else:
                         identity = -1
                    else:
                        identity = -1
                    if identity not in identities:
                        identities.append(identity)
                    b = datetime.datetime.now()
                    c = b-a 
                if not training:
                   bl = (bb.left(), bb.bottom())
                   tr = (bb.right(), bb.top())
                   cv2.rectangle(annotatedFrame, bl, tr, color=(153, 255, 204),
                              thickness=3)
                   
                   if identity == -1:
                     if len(people) == 1:
                        uuid = people[0].get("uuid")
                        name = people[0].get("name")
                     else:
                        uuid = ""
                        name = "anonymous"
                   else:
                        uuid = people[identity].get("uuid")
                        name = people[identity].get("name")

       if not training:
            msg = {
                "type": "INFO",
                "name": name,
                "rect":{"x":bb.left(),"y":bb.top(),"width":(bb.right()-bb.left()),"height":(bb.bottom()--bb.top())},
                "uuid":uuid,  
                "emotion":"happy"
            }
            
            if(client is not None):
              client.sendMessage(str.encode(json.dumps(msg)))
           
            retval, buffer_img= cv2.imencode('.png', annotatedFrame)

            tmp = {
                "type": "FRAME",
                "data":  base64.b64encode(buffer_img).decode('ascii')
            }

            if(client is not None):
              client.sendMessage(str.encode(json.dumps(tmp)))
 
  else:
       retval, buffer_img= cv2.imencode('.png', annotatedFrame)
       tmp = {
          "type": "FRAME",
          "data":   base64.b64encode(buffer_img).decode('ascii')
       }

       if(client is not None):
           client.sendMessage(str.encode(json.dumps(tmp))) 

class ServerProtocol(WebSocketServerProtocol):
    def __init__(self):
        super(ServerProtocol, self).__init__()
        
    def onConnect(self, request):
        print("Client connecting: {0}".format(request.peer))
        #TODO Fix this Dirty Hack
        global client;
        client=self;
     

    def onOpen(self):
        print("WebSocket connection open.")
        msg = {
                  "type": "PEOPLE",
                  "list": people
        }
        self.sendMessage(str.encode(json.dumps(msg)))
    def onMessage(self, payload, isBinary):
        global training;
        global people;
        #TODO Process Received Messages
        raw = payload.decode('utf8')

        msg = json.loads(raw)
        if msg['type'] == "TRAINING":
          training = True
          if not training:
             trainSVM()
        elif msg['type'] == "ADD": 
            info = msg['info'];
            name = info['name'] +" "+ info['surname'];
            people.append({"uuid":msg['uuid'],"name":name})
            file_people = open('people_new', 'wb') 
            pickle.dump(people, file_people)	
              

    def onClose(self, wasClean, code, reason):
        print("WebSocket connection closed: {0}".format(reason))
    




def runImageProcessing():
    ret, frame = video_capture.read()
    infer(frame)

def main(reactor):
    loadPickle() 
    factory = WebSocketServerFactory()
    factory.protocol = ServerProtocol
    reactor.listenTCP(9000, factory)

    l = task.LoopingCall(runImageProcessing)
    l.start(0.05)
   
    return defer.Deferred()


if __name__ == '__main__':
    task.react(main)
